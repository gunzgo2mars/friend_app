import { Component } from '@angular/core';
import { NavController , AlertController } from '@ionic/angular';
import { FetchdataService } from '../fetchdata.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
  userEmail : string
  userPassword : string
  
  constructor(public navCtrl : NavController , private fetchData : FetchdataService, private alertCtrl : AlertController) {}


  alertModal = async (message:string) => {

    const alert = await this.alertCtrl.create({

      header : 'Result',
      message : message,
      buttons : ['OK']

    })

    await alert.present()

  }

  requestLogin = async () => {
  
    const post = await this.fetchData.sendData(this.userEmail , this.userPassword , 'login')
    await post.subscribe(data => {
      
      const response = data.json()
      if(response.error == false) {
        
        this.fetchData.setKey(this.userEmail)
        this.navCtrl.navigateForward('list')

      } else {
        
        this.alertModal(response.message)

      }

    })

  }

  goToSignUp = () => this.navCtrl.navigateForward('signup')

}
