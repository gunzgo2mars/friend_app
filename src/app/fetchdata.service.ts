import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class FetchdataService {

  constructor(private http : Http) { }

  key : string
  
  setKey = (key : string) => { this.key = key }
  getKey = () => { return this.key }


  sendData = async (userEmail:string , userPassword:string , sendType:string) => {
    
    if(sendType == 'login') {
    
      return this.http.post('https://gunz-exam-api.herokuapp.com/api/v1/data/requestLoginByUser' , {
          
        userEmail : userEmail,
        userPassword : userPassword
          
      })

    }

    if(sendType == 'signup') {

      return this.http.post('https://gunz-exam-api.herokuapp.com/api/v1/data/createNewUser' , {
        
        userEmail : userEmail,
        userPassword : userPassword

      })

    }

  }

  sendDataNewfriend = async (friendEmail:string , friendName:string , userEmail:string) => {

    return this.http.post('https://gunz-exam-api.herokuapp.com/api/v1/data/createNewFriend' , {

      userEmail : userEmail,
      friendEmail : friendEmail,
      friendName : friendName

    })

  }

  fetchFriendByEmail = async (userEmail:string) => {

    return this.http.post('https://gunz-exam-api.herokuapp.com/api/v1/data/getFriendByUserEmail' , {

      userEmail : userEmail

    })

  }





}
