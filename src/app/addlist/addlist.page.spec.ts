import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddlistPage } from './addlist.page';

describe('AddlistPage', () => {
  let component: AddlistPage;
  let fixture: ComponentFixture<AddlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
