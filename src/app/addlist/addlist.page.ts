import { Component, OnInit } from '@angular/core';
import { AlertController , NavController } from '@ionic/angular'
import { FetchdataService } from '../fetchdata.service'

@Component({
  selector: 'app-addlist',
  templateUrl: './addlist.page.html',
  styleUrls: ['./addlist.page.scss'],
})
export class AddlistPage implements OnInit {

  friendEmail : string
  friendName : string
  keyEmail : string

  constructor(private navCtrl : NavController, private alertCtrl : AlertController , private fetchData : FetchdataService) {
    
    this.keyEmail = this.fetchData.getKey()

  }

  alertModal = async (message:string) => {

    const alert = await this.alertCtrl.create({

      header : 'Result',
      message : message,
      buttons : ['OK']

    })

    await alert.present()

  }

  ngOnInit() {
  }

  sendFriendInfo = async () => {
  
    const post = await this.fetchData.sendDataNewfriend(this.friendEmail , this.friendName , this.keyEmail)
    await post.subscribe(data => {

      const response = data.json()

      if(response.error == false) {
        
        this.alertModal(response.message)
        
      } else {
        this.alertModal(response.message)
      }

    })

  }

  goBack = () => this.navCtrl.goBack()



}
