import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular'
import { FetchdataService } from '../fetchdata.service'


@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
    
  keyEmail : string
  friendList : any = []
  constructor(private fetchData : FetchdataService , private navCtrl : NavController) {
    
    this.keyEmail = this.fetchData.getKey()
    this.getList()

  }

  ngOnInit() {
  }

  async doRefresh(event) {
    console.log('Begin async operation');
    await this.getList()
    setTimeout(() => {
      event.target.complete()
    }, 2000);
  }

  getList = async () => {
  
    const post = await this.fetchData.fetchFriendByEmail(this.keyEmail)
    await post.subscribe(data => {
    
      this.friendList = data.json()

    })
    

  }
  
  goToAddNewFriend = () => this.navCtrl.navigateForward('addlist')

}
